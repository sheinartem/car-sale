class RemoveColumnFromCars < ActiveRecord::Migration[6.1]
  def change
    remove_column :cars, :name
  end
end
