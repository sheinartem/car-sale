class CreateServiceSchedules < ActiveRecord::Migration[6.1]
  def change
    create_table :service_schedules do |t|
      t.references :car, foreign_key: true
      t.references :manager, foreign_key: true

      t.timestamp :service_date

      t.timestamps
    end
  end
end
