class RenameSurnameFromClients < ActiveRecord::Migration[6.1]
  def change
    rename_column :clients, :surname, :last_name
  end
end
