class AddColumnToServiceSchedule < ActiveRecord::Migration[6.1]
  def change
    add_column :service_schedules, :service_id, :string
  end
end
