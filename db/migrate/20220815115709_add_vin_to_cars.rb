class AddVinToCars < ActiveRecord::Migration[6.1]
  def change
    add_column :cars, :vin, :string, uniqueness: true
  end
end
