class RemovePurchaseFromClients < ActiveRecord::Migration[6.1]
  def change
    remove_column :clients, :purchase
  end
end
