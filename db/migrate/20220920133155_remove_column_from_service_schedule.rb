class RemoveColumnFromServiceSchedule < ActiveRecord::Migration[6.1]
  def change
    remove_column :service_schedules, :service_id
  end
end
