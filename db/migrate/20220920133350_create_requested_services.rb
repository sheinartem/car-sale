class CreateRequestedServices < ActiveRecord::Migration[6.1]
  def change
    create_table :requested_services do |t|
      t.references :service, foreign_key: true  
      t.references :service_schedule, foreign_key: true

      t.timestamps
    end
  end
end
