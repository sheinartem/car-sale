class CreateSales < ActiveRecord::Migration[6.1]
  def change
    create_table :sales do |t|
      t.references :car, foreign_key: true
      t.references :client, foreign_key: true
      t.references :manager, foreign_key: true
      
      t.timestamp :purchase_date
      
      t.timestamps
    end

  end
end
