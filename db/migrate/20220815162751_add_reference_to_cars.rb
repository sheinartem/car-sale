class AddReferenceToCars < ActiveRecord::Migration[6.1]
  def change
    add_reference :cars, :car_model, foreign_key: true
  end
end
