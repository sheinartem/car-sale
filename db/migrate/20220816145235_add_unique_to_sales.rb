class AddUniqueToSales < ActiveRecord::Migration[6.1]
  def change
    remove_index :sales, :car_id
    add_index :sales, :car_id, unique: true 
  end
end
