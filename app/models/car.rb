class Car < ActiveRecord::Base
    validates :colour, presence: true
    validates :price, presence: true
    validates :vin, presence: true, uniqueness: true

    belongs_to :car_model
    has_one :sale

    def name 
        "#{car_model.name_with_manufacturer} (VIN: #{vin})"
    end

    scope :recent, -> {order(created_at: :desc).limit(5) }
    scope :available, -> {left_outer_joins(:sale).where("sales.car_id is null")}
end 