class CarModel < ActiveRecord::Base
    belongs_to :manufacturer

    def name_with_manufacturer
        "#{manufacturer.name} #{name}"
    end
end