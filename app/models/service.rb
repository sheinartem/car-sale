class Service < ActiveRecord::Base
    has_many :requested_services
    has_many :service_schedules, through: :requested_services
end
