class Client < ActiveRecord::Base

    validates :first_name, presence: true, length: {minimum: 2}
    validates :last_name, presence: true, length: {minimum: 2}

    def full_name
        "#{first_name} #{last_name}"
    end 

    scope :recent, -> { order(created_at: :desc).limit(5) }                                                                                     
end