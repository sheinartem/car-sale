class ServiceSchedule < ActiveRecord::Base
    belongs_to :car
    belongs_to :manager
    has_many :requested_services, dependent: :destroy
    has_many :services, through: :requested_services

    scope :incoming, -> { where("service_date >= ?", Time.current.beginning_of_day).order(service_date: :asc).limit(5) }
end