class Sale < ActiveRecord::Base
    belongs_to :car
    belongs_to :client
    belongs_to :manager

    validates :car_id, presence: true, uniqueness: true
 
    scope :recent, -> { order(purchase_date: :asc).limit(5) }
    scope :top_managers, -> do
        group(:manager_id)
            .select(:manager_id)
            .order(count: :desc)
            .limit(5)
            .count
            .each_with_object([]) do |(manager_id, count), res| 
                res << {
                    full_name: Manager.find(manager_id).full_name, 
                    id: manager_id, 
                    count: count
                }
            end
    end
end
