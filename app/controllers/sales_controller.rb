class SalesController < ApplicationController
    def index
        @sales = Sale.all
    end

    def show  
        @sale = Sale.find(params[:id])
    end

    def new
        @sale = Sale.new
        fill_dictionaries
    end

    def create
        @sale = Sale.new(sale_params)

        if @sale.save
            redirect_to @sale
        else
            fill_dictionaries
            render :new
        end
    end

    def edit
        @sale = Sale.find(params[:id])
        fill_dictionaries
    end

    def update
        @sale = Sale.find(params[:id])
        fill_dictionaries
        

        if @sale.update(sale_params)
            redirect_to @sale
        else
            render :new
        end
    end

    def destroy
        @sale = Sale.find(params[:id])
        @sale.destroy

        redirect_to root_path
    end

    private

    def sale_params
        params.require(:sale).permit(:car_id, :client_id, :manager_id, :purchase_date)
    end

    def fill_dictionaries
        @cars = Car.all
        @available_cars = Car.available
        @clients = Client.all
        @managers = Manager.all
    end
end

