class CarModelsController < ApplicationController
    def index
        @car_models = CarModel.all
    end

    def show
        @car_model = CarModel.find(params[:id])
    end

    def new
        @car_model = CarModel.new
        fill_dictionaries
    end

    def create
        @car_model = CarModel.new(car_model_params)
    
        if @car_model.save
          redirect_to @car_model
        else
          render :new
        end
    end

    def edit
        @car_model = CarModel.find(params[:id])
        fill_dictionaries
    end
    
    def update
        @car_model = CarModel.find(params[:id])
        
        if @car_model.update(car_model_params)
            redirect_to @car_model
        else
            render :edit
        end
    end 

    def destroy
        car_model = CarModel.find(params[:id])
        car_model.destroy
    
        redirect_to root_path
    end
      
    private
    
    def car_model_params
      params.require(:car_model).permit(:manufacturer_id, :name)
    end

    def fill_dictionaries
        @manufacturers = Manufacturer.all
    end

end
