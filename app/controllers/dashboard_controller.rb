class DashboardController < ApplicationController
    def index
        @sales = Sale.recent
        @clients = Client.recent
        @managers = Sale.top_managers
        @cars = Car.recent
        @services_schedule = ServiceSchedule.incoming
    end
end
