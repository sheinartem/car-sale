class ServiceSchedulesController < ApplicationController
    def index
        @service_schedules = ServiceSchedule.all 
    end

    def show
        @service_schedule = ServiceSchedule.find(params[:id])
    end

    def new
        @service_schedule = ServiceSchedule.new
        fill_dictionaries
    end

    def create         
        @service_schedule = ServiceSchedule.new(service_schedule_params)
        (params[:service_schedule][:service_ids] - [""]).each do |service_id|
            @service_schedule.requested_services << RequestedService.new(service_id: service_id)
        end

        if @service_schedule.save
            redirect_to @service_schedule
        else
            fill_dictionaries
            render :new
        end
    end

    def edit
        @service_schedule = ServiceSchedule.find(params[:id])
        fill_dictionaries
    end
    
    def update
        @service_schedule = ServiceSchedule.find(params[:id])

        @service_schedule.requested_services.destroy_all
        (params[:service_schedule][:service_ids] - [""]).each do |service_id|
            @service_schedule.requested_services << RequestedService.new(service_id: service_id)
        end

        
        if @service_schedule.update(service_schedule_params)
            redirect_to @service_schedule
        else
            fill_dictionaries
            render :edit
        end
    end 

    def destroy
        service_schedule = ServiceSchedule.find(params[:id])
        service_schedule.destroy
    
        redirect_to service_schedules_path
    end
      
    private
    
    def service_schedule_params
      params.require(:service_schedule).permit(:car_id, :manager_id, :service_date)
    end

    def fill_dictionaries
        @cars = Car.all
        @managers = Manager.all
        @services = Service.all
    end
end
