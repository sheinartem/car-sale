class CarsController < ApplicationController
    def index
        @cars = Car.all
    end

    def show
        @car = Car.find(params[:id])
    end

    def new
        @car = Car.new
        fill_dictionaries
    end

    def create
        @car = Car.new(car_params)
    
        if @car.save
          redirect_to @car
        else
          render :new
        end
    end

    def edit
        @car = Car.find(params[:id])
        fill_dictionaries
    end
    
    def update
        @car = Car.find(params[:id])
        
        if @car.update(car_params)
            redirect_to @car
        else
            render :edit
        end
    end 

    def destroy
        @car = Car.find(params[:id])
        @car.destroy
    
        redirect_to root_path
    end
      
    private
    
    def car_params
      params.require(:car).permit(:colour, :price, :vin, :car_model_id)
    end

    def fill_dictionaries
        @car_models = CarModel.all
        @manufacturers = Manufacturer.all
    end

end