Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'dashboard#index' 

  resources :cars
  resources :clients
  resources :managers
  resources :sales
  resources :manufacturers
  resources :car_models
  resources :services
  resources :service_schedules
  resources :requested_services
  
end
